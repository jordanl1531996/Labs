
import java.util.Scanner;

//  Author: Adam Morrin Date: September 2016
//	Write a program in Java called reverseString to read in a
//	string from the keyboard into a variable using the Scanner class. 
//	It should then print out the letters of the string in reverse order.


public class reverseString
{
	public static void main (String args[])
	{
		int	index;
		
		
		Scanner sc = new Scanner(System.in);
		//input sop	
		System.out.print("Please enter some text : ");
		String text = sc.nextLine();				
		//for loop to print all characters from the end to the start
    	for(index = text.length()-1 ; index >= 0 ;index--)
	    	{
				System.out.print(text.charAt(index)) ;
	    	}
		System.out.println("");

		

		}
}